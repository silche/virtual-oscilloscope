### Virtual Oscilloscope

2nd year bachelor's course project in discipline "Object-oriented programming". Main goal of the project: to study OOP in practice.


© 07/2015



<img width="600" alt="Oscilloscope simulation C# version 2" align="center" src="https://gitlab.com/silche/oscilloscope-simulation/-/raw/master/screenshots/version2/1.png">
 

> Version 2. Interactive. Moving the signal using its direct capture



<img width="500" alt="Oscilloscope simulation C# version 1" src="https://gitlab.com/silche/oscilloscope-simulation/-/raw/master/screenshots/version1/3.png">


> Version 1. Interaction through controls
